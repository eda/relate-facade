# Relate-IQ npm Documentation
For usage with EDA admissions prototype

* [SalesforceIQ API Docs](https://api.salesforceiq.com) - this is NOT the same as salesforce API. This is the API you are looking for.

**IMPORTANT FOR NPM TO WORK**
This Npm uses [dotenv](https://www.npmjs.com/package/dotenv) and [fs](https://nodejs.org/api/fs.html) for handling credentials for relateiq authentication.
Create a .env file and set the following
* R_APIKEY="key"
* R_APISECRET="secret"
Initialize dot env
```
require('dotenv').load();
```

Then initialize relate;

```
var credentials = {
  user: process.env.R_APIKEY,
  pass: process.env.R_APISECRET
};
var relate = require('../index.js')(testCredentials);

```


One initialized the following functions are available to be used:


## getContact function
```
function getContact(identifier,callback)
```
identifier can be either a relateiq ID or email, callback returns the raw response from relate

## getContacts function

```
function getContacts(callback)

```
Returns all contacts


## createContact function
```
function createContact(contact,callback)

```
Takes a JSON contact object which requires the following fields:

```
{
  "properties":{
    "name":[
      {
        "value": example student
      }
    ],
    "email":[
      {
        "value":example name
      }
    ],
    "phone":[
      {
        "value":example phone
      }
    ],
    "address":[
      {
        "value":example address
      }
    ]
  }
}

```

Creates a contact in relateIq contact. Callback returns response.

## updateContact

Updates an existing student object. Note a relateIQ contact ID must be supplied before the properties property, all other properties the same as the createContact contact object

```
"id": relateiq contact ID,
"properties": {
  ...
}
```

## getLists

Returns all Lists in relate
f

## getList
```
function getList(relateListId, callback)
```
Returns one list and its values


Current student list values as of 17/09 are -

```
{ id: 'example student id',
  title: 'Students',
  listType: 'contact',
  modifiedDate: 0,
  fields:
   [ { id: '32',
       name: 'Tree Note',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '84',
       name: 'Preferred name',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' },
     { id: 'led',
       name: 'Last Event Date (sort)',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'DateTime' },
     { id: 'process_created_date',
       name: 'Created Date',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'DateTime' },
     { id: '1',
       name: 'Owner',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'User' },
     { id: 'process_close_date',
       name: 'Close Date',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'DateTime' },
     { id: '8',
       name: 'Prep',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '10',
       name: 'Accpt Letter',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '12',
       name: 'Github',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '18',
       name: 'Google',
       listOptions: [Object],
       isMultiSelect: true,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '16',
       name: 'Github Sent',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '17',
       name: 'Slack',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '20',
       name: 'Treehouse',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Date' },
     { id: '24',
       name: 'Previous Cohorts',
       listOptions: [Object],
       isMultiSelect: true,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '26',
       name: 'Orientation',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '30',
       name: 'Cohort (Confirmed)',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '27',
       name: 'Gender',
       listOptions: [Object],
       isMultiSelect: true,
       isEditable: false,
       isLinkedField: true,
       dataType: 'List' },
     { id: '36',
       name: 'Invoice sent',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Date' },
     { id: '38',
       name: 'T&C\'s signed',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '40',
       name: 'Expected Revenue',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Numeric' },
     { id: '103',
       name: 'Next of Kin Phone',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '105',
       name: 'Next of Kin Email',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '63',
       name: 'U1W1',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '65',
       name: 'U1W3',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '64',
       name: 'U1W2',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '69',
       name: 'U3W7',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '71',
       name: 'U3W9',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '67',
       name: 'U2W5',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '68',
       name: 'U2W6',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '70',
       name: 'U3W8',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '66',
       name: 'U2W4',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '81',
       name: 'Cohort (Pending)',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' },
     { id: '102',
       name: 'Next of Kin Name',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '94',
       name: 'Scholarship $',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Numeric' },
     { id: '99',
       name: 'Discount $',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Numeric' },
     { id: '95',
       name: 'Scholarship Type',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '93',
       name: 'Scholarship Status',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '98',
       name: 'Discount Description',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '110',
       name: 'Quick note',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '112',
       name: 'Tech Competency',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '0',
       name: 'Status',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '114',
       name: 'City (manual)',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '117',
       name: 'Marae',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' },
     { id: '118',
       name: 'Te Ao Maori involvement',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' },
     { id: '115',
       name: 'Iwi',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' },
     { id: '116',
       name: 'Hapu',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' },
     { id: '120',
       name: 'Admissions',
       listOptions: [Object],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '127',
       name: 'Payment Plan',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Text' },
     { id: '123',
       name: 'City',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' },
     { id: '131',
       name: 'Discount Type',
       listOptions: [Object],
       isMultiSelect: true,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '135',
       name: 'Payments',
       listOptions: [Object],
       isMultiSelect: true,
       isEditable: true,
       isLinkedField: false,
       dataType: 'List' },
     { id: '137',
       name: 'Loan $',
       listOptions: [],
       isMultiSelect: false,
       isEditable: true,
       isLinkedField: false,
       dataType: 'Numeric' },
     { id: '138',
       name: 'Email',
       listOptions: [],
       isMultiSelect: false,
       isEditable: false,
       isLinkedField: true,
       dataType: 'Text' } ]

```

Current fields with list items

```
Prep
[ { id: '0', display: 'Y' },
  { id: '1', display: 'N' },
  { id: '2', display: 'chased' } ]
Accpt Letter
[ { id: '0', display: 'Y' }, { id: '1', display: 'N' } ]
Google
[ { id: '0', display: 'Y' }, { id: '1', display: 'N' } ]
Github Sent
[ { id: '0', display: 'Y' }, { id: '1', display: 'N' } ]
Slack
[ { id: '0', display: 'Y' }, { id: '1', display: 'N' } ]
Previous Cohorts
[ { id: '0', display: '2014_GSK' },
  { id: '1', display: '2014_Kereru' },
  { id: '2', display: '2014_Weka' },
  { id: '3', display: '2014_Pukeko' },
  { id: '4', display: '2014_Kakapo' },
  { id: '5', display: '2014_Karearea' },
  { id: '6', display: '2015_Weka' },
  { id: '7', display: '2015_Ruru' },
  { id: '8', display: '2015_Pukeko' },
  { id: '9', display: '2015_Kakapo' },
  { id: '10', display: '2015_Karearea' },
  { id: '11', display: '2016_Kakapo' },
  { id: '12', display: '2015_Kahu' },
  { id: '13', display: '2015_Miromiro' } ]
Orientation
[ { id: '0', display: 'Y' }, { id: '1', display: 'N' } ]
Cohort (Confirmed)
[ { id: '10', display: '2015_Pukeko' },
  { id: '0', display: '2015_Kahu' },
  { id: '1', display: '2015_Ruru' },
  { id: '2', display: '2015_Weka' },
  { id: '3', display: '2016_Pukeko' },
  { id: '4', display: '2016_Kakapo' },
  { id: '5', display: '2016_Karearea' },
  { id: '6', display: '2015_Hihi' },
  { id: '7', display: '2016_Kotare' },
  { id: '8', display: 'Undecided' },
  { id: '9', display: 'Withdrawn' } ]
Gender
[ { id: '0', display: 'Female' },
  { id: '1', display: 'Male' },
  { id: '2', display: 'Other' } ]
T&C's signed
[ { id: '0', display: 'Y' },
  { id: '1', display: 'Sent' },
  { id: '2', display: 'N' } ]
Scholarship Type
[ { id: '0', display: 'Te Uru Rangi' },
  { id: '3', display: 'Ruby' } ]
Scholarship Status
[ { id: '0', display: 'Applied' },
  { id: '1', display: 'Granted' },
  { id: '2', display: 'Declined' } ]
Tech Competency
[ { id: '0', display: 'Lapping' },
  { id: '1', display: 'Swimming' },
  { id: '2', display: 'Treading' },
  { id: '3', display: 'Drowning' } ]
Status
[ { id: '9', display: 'Admissions' },
  { id: '0', display: 'Student' },
  { id: '2', display: 'Graduated' },
  { id: '4', display: 'Withdrawn' } ]
Admissions
[ { id: '1', display: 'In Progress' },
  { id: '2', display: 'Complete' },
  { id: '3', display: 'On hold' },
  { id: '4', display: 'Test'} ]
Discount Type
[ { id: '0', display: 'Early Bird' },
  { id: '1', display: 'Diversity' },
  { id: '2', display: 'Work for Study' },
  { id: '3', display: 'Staff' },
  { id: '4', display: 'Gift Economy' },
  { id: '5', display: 'Enspiral' } ]
Payments
[ { id: '0', display: 'Dep Sent' },
  { id: '1', display: 'Dep Paid' },
  { id: '2', display: 'Full Inv Sent' },
  { id: '3', display: 'Full Inv Paid' },
  { id: '4', display: 'P. Plan Sent' },
  { id: '5', display: 'P. Plan in Progress' },
  { id: '6', display: 'P. Plan Paid' },
  { id: '7', display: 'On hold' },
  { id: '8', display: 'FOC' },
  { id: '9', display: 'Loan in Progress' },
  { id: '10', display: 'Partial Payment' } ]

```

## getListItems

```
function getListItems(relateListId, callback)

```

returns all contacts in a list - Limited to 50 items due to Relate IQ response

## studentsToOnboard
```
function studentsToOnboard(callback)
```

### Usage

Has the student list id hardcoded as this is the only list we will be currently using. If this changes in the future it will need to be updated.

Will check relate students list for students that have the field admissions set to onboarding( meaning they are ready for the paperwork to be sent).

### Return value

Returns an array of student objects - NOTE field values may differ to example due to some students not having all the fields filled out.

#### Regularly used fieldValues
* 12 - Name
* 36 - Invoice Sent
* 38 - T&C signed
* 138 - Email
* 30 - Cohort Confirmed
```
  [ { id: '10', display: '2015_Pukeko' },
    { id: '0', display: '2015_Kahu' },
    { id: '1', display: '2015_Ruru' },
    { id: '2', display: '2015_Weka' },
    { id: '3', display: '2016_Pukeko' },
    { id: '4', display: '2016_Kakapo' },
    { id: '5', display: '2016_Karearea' },
    { id: '6', display: '2015_Hihi' },
    { id: '7', display: '2016_Kotare' },
    { id: '8', display: 'Undecided' },
    { id: '9', display: 'Withdrawn' } ]

```

```
[
{ id: 'example id',
    listId: 'example list id',
    version: 1,
    createdDate: 1437538377000,
    modifiedDate: 1442299328287,
    name: 'Mr Example',
    accountId: null,
    contactIds: [ 'example id' ],
    fieldValues:
     { '0': [Object],
       '1': [Object],
       '8': [Object],
       '10': [Object],
       '12': [Object],
       '24': [Object],
       '30': [Object],
       '38': [Object],
       '40': [Object],
       '63': [Object],
       '64': [Object],
       '65': [Object],
       '66': [Object],
       '67': [Object],
       '68': [Object],
       '69': [Object],
       '81': [Object],
       '93': [Object],
       '94': [Object],
       '95': [Object],
       '99': [Object],
       '110': [Object],
       '112': [Object],
       '114': [Object],
       '120': [Object],
       '135': [Object],
       '138': [Object],
       process_close_date: [Object],
       process_created_date: [Object] },
    linkedItemIds: { 'list.example id': [Object] } },
    ...
]
```

## updateStudentList
```
function(updateStudentList(listItemId, updatedListItem, callback))
```

listItemId - Id field passed through from a list Item
updatedListItem -
Currently only tested the following structure, id is the key of the field value to update.
Also have only tested a text field, not listitem or date.

```
var updatedListItem = {
  fieldValues:{
    'id':[
      {
        "raw":example
      }
    ]
  }
};
```

Will return code:200 if updated successfully

Updates the an List Item on the student list. Used to update fields

Coming soon...


## listOfStudents

Coming soon ...
