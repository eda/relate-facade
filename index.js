var unirest = require('unirest');
var base_uri = "https://api.relateiq.com/v2/contacts";
var listUri = "https://api.relateiq.com/v2/lists";
var headers ={
      'Accept':'application/json',
      'Content-Type':'application/json'
    };
var user;
var pass;
var auth;
var _ = require('lodash');


var initialize = function initialize(relateCredentials){
  auth = {
    user: relateCredentials.user,
    pass: relateCredentials.pass,
    sendImmediately:true
  };
  return {
    getContacts: getContacts,
    getContact: getContact,
    createContact: createContact,
    updateContact: updateContact,
    getLists: getLists,
    getListFields: getListFields,
    getListItems: getListItems,
    getCohortStudents: getCohortStudents,
    checkForOnboarding: checkForOnboarding,
    studentsToOnboard: studentsToOnboard,
    checkForCohortListField: checkForCohortListField,
    returnListValues: returnListValues,
    updateListItem: updateListItem,
    mailMergeStudents: mailMergeStudents,
    updateByEmail: updateByEmail
  };
};

function _uniGet(uri,callback){
  unirest.get(uri)
    .auth(auth)
    .header(headers)
    .end(function(response){
      callback(response.body);
    });
}

function _uniPost(uri,info,callback){
  unirest.post(uri)
    .auth(auth)
    .header(headers)
    .send(info)
    .end(function(response){
      callback(response);
    });
}

function _uniPut(contactId, info, callback){
  unirest.put(base_uri + "/" + contactId)
  .auth(auth)
  .header(headers)
  .send(info)
  .end(function(response){
    callback(response);
  });
}

function _listUpdatePut(listId, listItemId, updatedListItem, callback){
  var listItemUpdateUrl = "https://api.salesforceiq.com/v2/lists/"+listId+"/listitems/" +listItemId;
  unirest.put(listItemUpdateUrl)
  .auth(auth)
  .header(headers)
  .send(updatedListItem)
  .end(function(response){
    callback(response);
  });
}
function _filterStudentsByEmail(students,studentEmail,callback){
  //arguments: array of students, email to update
  students.forEach(function(student){
    if(studentEmail === (student.fieldValues['150'][0].raw)){
      console.log(student);
      console.log(student.fieldValues['150'][0].raw);
      callback(student.id);
    }
  });
}

var getContacts = function getContacts (callback){
    _uniGet(base_uri,function(response){
      callback(response);
    });
};

var getContact = function getContact(identifier,callback){
  var contact_url = "/";
  if(identifier.indexOf("@") != -1){
    contact_url ="?properties.email=";
  }
  url = base_uri + contact_url + identifier;
    _uniGet(url,function(response){
      callback(response);
    });
};

var createContact = function createContact(contact, callback){
  _uniPost(base_uri,contact,function(response){
    callback(response);
  });
};

var updateContact = function updateContact(contactId, contactInfo,callback){
  _uniPut(contactId,contactInfo,function(response){
    callback(response);
  });
};

var getListFields = function getListFields(id,callback){
  _uniGet(listUri + "/" + id, function(response){
    callback(response);
  });
};

var getLists = function getLists(callback){
  _uniGet(listUri + "/?_start=0",function(response){
    callback(response);
  });
};

var getListItems = function getListItems(id, callback) {
  var start = 0
  var limit = 200
  var totalResponse = { objects: [] }

  getListItemsWithLimit(start, limit)

  function getListItemsWithLimit(start, limit) {
    _uniGet(listUri + "/" + id + "/listitems?_start=" + start + "&_limit=" + limit, function(response) {
      // console.log("response: ", response)
      totalResponse.objects = totalResponse.objects.concat(response.objects)
      console.log("getListItemsWithLimit response.objects.length: ", response.objects.length)
      if (response.objects.length == limit) {
        var newStart = start + limit
        getListItemsWithLimit(newStart, limit)
      } else {
        callback(totalResponse)
      }
    })
  }
}

var getCohortStudents = function getCohortStudents(cohort,callback){
  var cohort = cohort.toLowerCase()
  var cohortRelateId = "55a70228e4b01fe8e5a3d93b";
  var cohortList = {
    "0":"kahu",
    "1":"ruru",
    "2":"weka"
  };
  var studentsObject = {cohort:cohort,students:[]};
  getListItems(cohortRelateId, function(response){
    response.objects.forEach(function(student){
      if(student.fieldValues['30']!== undefined){
        var cohortId = student.fieldValues['30'][0].raw;
        if(cohortList[cohortId]===cohort){
          studentsObject.students.push({student:student});
        }
      }
    });
    callback(studentsObject);
  });
};

var checkForOnboarding = function checkForOnboarding(callback){
  var studentListId = "55a70228e4b01fe8e5a3d93b";
  getListFields(studentListId,function(response){
    var listFieldsCollection = response.fields;
    pullOnboardField(listFieldsCollection);
  });
  function pullOnboardField(listFields){
    //field name - This needs to be changed
    var requiredFieldName = 'Admissions';
    varFieldObject = function(){
      listFields.forEach(function(field){
        if(field.name === requiredFieldName){
          callback(field);
        }
      });
    }();
  }
};


var checkForCohortListField = function checkForCohortListField(callback){
  var studentListId = "55a70228e4b01fe8e5a3d93b";
  getListFields(studentListId,function(response){
    var listFieldsCollection = response.fields;
    pullOnboardField(listFieldsCollection);
  });
  function pullOnboardField(listFields){
    //field name - This needs to be changed
    var requiredFieldName = 'Cohort (Confirmed)';
    varFieldObject = function(){
      listFields.forEach(function(field){
        if(field.name === requiredFieldName){
          callback(field);
        }
      });
    }();
  }
};

// var returnCohort = function returnCohortName(id,callback){
//   checkForCohortListField(function(response){
//     response.
//   });
// }

//returns an array of students who have status admissions - 3.initiate adm tasks
var studentsToOnboard = function studentsToOnboard(callback1){
  var studentListId = "55a70228e4b01fe8e5a3d93b";
  //get List fields
  //getList of students to be onboarded
  checkForOnboarding(function(response){
    // console.log(response);
    ToOnboard(response);
  });

//need some error handling in here in case list is empty
  function ToOnboard(fieldObject){
    var fieldName = fieldObject.name,
        fieldId = fieldObject.id;
    getListItems(studentListId, function(response){
      // console.log(response);
      callback1(_.filter(response.objects, function(student) {
        //list option from admissions list - 8 is initiate adm tasks
        return student.fieldValues.hasOwnProperty(fieldId) === true && (student.fieldValues[fieldId][0].raw)==='8';
      }));
    });
  }
};


var mailMergeStudents = function mailMergeStudents(err,success){
  var studentListId = "55a70228e4b01fe8e5a3d93b";
  //get List fields
  //getList of students to be onboarded
  checkForOnboarding(function(response){
    ToOnboard(response);
  });

  //need some error handling in here in case list is empty
  function ToOnboard(fieldObject){
    var fieldName = fieldObject.name,
        fieldId = fieldObject.id;
    getListItems(studentListId, function(response){
      success(_.filter(response.objects, function(student) {
        //field Value is 6 for initiate
        return student.fieldValues.hasOwnProperty(fieldId) === true && (student.fieldValues[fieldId][0].raw)==='6';
      }));
    });
  }
};

var returnListValues = function returnListValues(reqField,error,success){
  //this will return object for a field value based on name
  var studentListId = "55a70228e4b01fe8e5a3d93b";
  getListFields(studentListId,function(response){
    var listFieldsCollection = response.fields;
    pullOnboardField(listFieldsCollection);
  });
  function pullOnboardField(listFields){
    //field name - This needs to be changed
    // var requiredFieldName = 'Admissions';
    varFieldObject = function(){
      listFields.forEach(function(field){
        if(field.name === reqField){
          success(field);
        }
      });
    }();
  }
};

var updateListItem = function updateListItem(updateItem, err,success){
  var listId = "55a70228e4b01fe8e5a3d93b";
  var updateItemField = updateItem.field;
  var fieldtype = function fieldtype(updateItemField){
    getListFields(listId, function(response){
       _.filter((response.fields),function(f){
        if(f.name === updateItem.field){
          if((f.dataType)=== 'List'){
            updateListField(updateItem,f);
          }
        //   else if((f.dataType)=== 'Date'){
        //   /// what is the format of the date that is going to be passed in here?
        //   //From xero npm
        // }
          else{
            updateTextField(updateItem,f.id);
          }
        }
      });
    });
  }();

  function updateTextField(updateTextValue,fieldId){
    console.log(updateTextValue);
    var updateItem = {
      fieldValues:{
      }
    };
    updateItem.fieldValues[fieldId]=[
      {
        'raw': updateTextValue.update
      }
    ];
   _listUpdatePut(listId, updateTextValue.studentListId,updateItem, function(response){
     console.log(updateItem);
      success(response);
    });

  }

  function updateListField(updateListObject, field){
    var listOption = function(){
      return(
        _.filter((field.listOptions), function(n){
          if((n.display)=== updateListObject.update){
            return n.id;
          }
        })
      );
    };

    var updateItem = {
      fieldValues:{
      }
    };
    updateItem.fieldValues[field.id]=[
      {
        'raw': listOption()[0].id
      }
    ];
    _listUpdatePut(listId, updateListObject.studentListId,updateItem, function(response){
      success("successfully updated");
     });
  }
};

var updateByEmail = function updateByEmail(email, fieldObject, error,success){
  var studentListId = "55a70228e4b01fe8e5a3d93b";
    getListItems(studentListId,function(response){
      var student ={
          field: fieldObject.field,
          update: fieldObject.update
      };
      _filterStudentsByEmail(response.objects,email, function(response){
        console.log(response);
        student.studentListId = response;
      });

      updateListItem(student,
        function(err){
          console.log(student);
          error(err);
        },
        function(done){
          console.log(student);
          success(done);
        });
    });
};

module.exports = initialize;
