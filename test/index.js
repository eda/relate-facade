var expect = require("chai").expect;
var request = require("request");
require('dotenv').load();
var testCredentials = {
  user: process.env.R_APIKEY,
  pass: process.env.R_APISECRET
};
var _ = require('lodash');
var relateNpm = require('../index.js')(testCredentials);
var studentListId = "55a70228e4b01fe8e5a3d93b";


describe('.getListItems', function() {

  // returns all items from student list
  it("is defined", function(done) {
    expect(relateNpm).to.have.property("getListItems")
    done()
  })

  it("returns all list items", function(done){
    this.timeout(20000)
    relateNpm.getListItems(studentListId, function(response) {
      console.log("list items response: ", response)
      console.log("list items response.objects.length: ", response.objects.length)
      // console.log("list items response[0].objects[0]: ", response[0].objects[0])
      // console.log("list items response[0].objects.length: ", response[0].objects.length)
      expect(response.objects[0].listId).to.eq(studentListId)
      done()
    })
  })
})

xdescribe('.getListFields', function() {
  var resp
  before(function(done){
    relateNpm.getListFields(studentListId, function(response){
      console.log(response)
    });
  });

  it("is defined", function(done){
    expect(relateNpm).to.have.property("getList");
    done();
  });
  xit("returns correct list", function(done){
    expect(resp.id).to.eq(studentListId);
    done();
  });
  xit("returns an array of fields", function(done){
    expect(resp.fields).to.be.a('array');
    expect(resp.fields).to.have.length.above(0);
    done();
  });
  it("returns field values for admissions", function(done){
  });
});


xdescribe(".checkForCohortListField ", function(done){
  it("returns admissions field", function(done){
    var OnboardingField = "Admissions";
    relateNpm.checkForCohortListField(function(response){
      console.log(response);
      expect(response.name).to.eq(OnboardingField);
      done();
    });
  });
});

xdescribe(".checkForOnboarding cohort", function(done){
  it("returns admissions field", function(done){
    var OnboardingField = "Admissions";
    relateNpm.checkForOnboarding(function(response){
      console.log(response);
      expect(response.name).to.eq(OnboardingField);
      done();
    });
  });
});


xdescribe('#getLists', function(){
  it("returns a list of lists", function(done){
    relateNpm.getLists(function(response){
      console.log(response);
      done();
    });
  });
});


xdescribe('.studentsToOnboard', function(){
  var resp;
  before(function(done){
    this.timeout(10000);
    relateNpm.studentsToOnboard(function(response){
      console.log(response);
      console.log(response[0].fieldValues['84'][0].raw);
      resp = response;
      done();
    });
  });

  it("returns a list of students with status:onboard", function(done){
      expect(resp[0].fieldValues['120'][0].raw).to.eql('1');
      done();
  });

    xdescribe("student", function(){
      var student;
      var studentId;
      before(function(done){
        student = resp[0];
        relateNpm.getContact(student.contactIds[0], function(response){
          studentId = response.id;
          done();
        });
      });

      it("has a name ", function(done){
        expect(student).to.have.property('name');
        done();
      });

      it("has an email", function(done){
        expect(student.fieldValues).to.have.deep.property('138');
        done();
      });
      it("has a valid contactId", function(done){
        expect(student.contactIds[0]).to.eql(studentId);
        done();
      });
    });
});

xdescribe(".returnListValues",function(){
  var resp,
  field = "Admissions";
  before(function(done){
    relateNpm.returnListValues(field,
      function(err){
        console.log(err);
      },
      function(success){
        console.log(success);
        resp = success;
        done();
      });
  });
  it("has correct field name",function(done){
    expect(resp.name).to.eq(field);
    done();
  });
  it("has list options",function(done){
    // expect(resp.listOptions).to.
  });
});


xdescribe(".updateListItem", function(){
  this.timeout(5000);
  var testStudent = {
    studentListId:"561d86c0e4b035e1092587de",
    field: "Admissions",
    update:"4. Adm tasks sent"
  };
  var resp;
  before(function(done){
    relateNpm.updateListItem(testStudent,
      function(err){
        console.log(err);
      },
      function(success){
        resp = success;
        console.log(success);
        done();
      });
  });
  it("does stuff", function(done){
    console.log("1");
    done();
  });
});

xdescribe(".mailMergeStudents", function(){
  var resp;
      this.timeout(10000);
  before(function(done){
    relateNpm.mailMergeStudents(
      function(err){
        console.log(err);
      },
      function(success){
        console.log(success);
        resp = success;
        done();
      }
    );
  });
  xit("returns an array of students", function(done){
    console.log("hello");
    done();
  });
});

xdescribe("updateByEmail", function(){
  var resp;
      fieldObject = {
        field: "T&C\'s signed",
        update:"Y"
      };
      this.timeout(10000);
  var studentEmailToUpdate = "kerwin.coleman91@gmail.com";
  before(function(done){
      relateNpm.updateByEmail(studentEmailToUpdate,fieldObject,
      function(err){
        console.log("Err"+ err);
        done();
      },
      function(response){
        var resp = response;
        console.log("success"+ response);
        done();
      });
  });
  it("updates correct field",function(done){
    console.log(resp);
    expect(resp.field).to.eql(fieldObject.field);
    expect(resp.update).to.eql(fieldObject.update);
    done();
  });
});
